# Aide et ressources de Demeter pour Synchrotron SOLEIL

[<img src="https://bruceravel.github.io/demeter/images/Demeter.png" width="200"/>](https://bruceravel.github.io/demeter/)

## Résumé

- Ce package regroupe un ensemble d'outils utilisé pour l'intégralité des données d'absorption des rayons x. Athéna permet de normaliser et préparer la données brute, Artémis permet de faire des fits de spectre EXAFS (signal utilisé en abs x pour retrouver les structures autour de l'atome excité pour les rayons x)
- Open source

## Sources

- Code source: https://github.com/bruceravel/demeter
- Documentation officielle: https://bruceravel.github.io/demeter/#docs

## Navigation rapide

| Tutoriaux | Ressources annexes | Page pan-data |
| - | - | - |
| [Tutoriel d'installation officiel](https://bruceravel.github.io/demeter/documents/SinglePage/demeter_nonroot.html) | [Mailing list](https://millenia.cars.aps.anl.gov/mailman/listinfo/ifeffit/) | [Documentation pan-data du logiciel](https://software.pan-data.eu/software/157/demeter) |

## Installation

- Systèmes d'exploitation supportés: Windows, MacOS
- Installation: Facile (tout se passe bien)

## Format de données

- en entrée: Il faut des fichiers texte au départ. Une fois un projet sauvegardé il peut être réouvert avec des préférences sauvés par le logiciel.
- en sortie: On récupère des fichiers texte ou images.
- sur un disque dur
